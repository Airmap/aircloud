Welcome to the Air Pollution Revolution


#######################################

To set up the webserver:

python3
pip3
mongodb

pip install:
django==1.10
mongoengine==0.9.0
pymongo==2.8

in mongo:
use airdb
db.createUser( { user: "admin",
                 pwd: "[PASSWORD]",
                 roles: [ "readWrite", "dbAdmin" ]
   }
)

To run it:

python3 manage.py runserver

#######################################

Device code lives in {ROOT}/device

To run:

	pip3 install requests

#######################################
