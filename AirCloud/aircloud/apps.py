from django.apps import AppConfig

class AircloudConfig(AppConfig):
    name = 'aircloud'
