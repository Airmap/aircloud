import sys
import pdb
from models import *
from mongoengine import *
from datetime import datetime, timedelta
import random
import time
connect('airdb')

NUM_POINTS = 100
DEVICE_ID = 12345678
# BOUNDS
LAT1=42.227880
LAT2=42.400820
LNG1=-71.191113
LNG2=-71.0429

if __name__=="__main__":
    lat_range = abs(LAT2-LAT1)
    lng_range = abs(LNG2-LNG1)
    lat = 42.339887# neu #42.323#dudley
    lng = -71.065 #bec#-71.089940#neu #-71.0822#dudley
    for i in range(NUM_POINTS):
        lat_change = (lat_range*(random.random()-.5))
        if lat_change > 0:
            lat+=lat_change%(abs(LAT1-lat)/5)
        else:
            lat-=abs(lat_change)%(abs(LAT2-lat)/5)

        lng_change = (lng_range*(random.random()-.5))%(min(abs(LNG1-lng),abs(LNG2-lng))/5)
        if lat_change > 0:
            lng+=lng_change%(abs(LNG1-lng)/5)
            print("adding to lng "+str(lng_change%(abs(LNG2-lng))))
        else:
            lng-=abs(lng_change)%(abs(LNG2-lng)/5)
            print("subbing from lng "+str(abs(lng_change)%(abs(LNG2-lng))))

        rand_gps = GPS(latitude = lat,
                       longitude = lng,
                       number_satellites=11,
                       run_status = 1,
                       fix_status = 1,
                       unix_time = time.time(),
                       altitude = random.randint(0,100),
                       altitude_units = 'm')
        rec = DeviceRecord(device_id = DEVICE_ID,
                           datetime = datetime.now(),
                           gps = rand_gps,
                           temperature = 22.0 + random.random()%0.8,
                           temperature_units = 'C',
                           humidity = 30+ random.randint(0,10),
                           humidity_units = '%',
                           pressure = 101000 + random.randint(0,1000),
                           pressure_units = 'Pa',
                           ozone_concentration = float(random.random()%0.2),
                           ozone_units = 'ppm',
                           pm25_concentration = random.random()%1, #TODO
                           pm25_units = 'ppm')
        rec.save()
