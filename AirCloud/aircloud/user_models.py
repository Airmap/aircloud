from mongoengine import *
from mongoengine.django.auth import User
class Auth:
    def __init__(self, idf):
        self.val = int(str(idf),16)
    def value_to_string(self,user):
        return str(hex(self.val))[2:]
    def __int__(self):
        return self.val

class Profile(User):
    # public_id = IntField()  -- if we ever want to include a public profile e.g. at aircloud.com/profile/[id]
    # API key is 32-character hex string
    api_key = StringField(max_length=32)
    # Device ID is 8-character hex string
    device_ids = ListField(StringField(max_length=8))
    date_joined = DateTimeField()
    is_contributor = BooleanField()
    number_logins = IntField()
    def __init__(self,*args,**kwargs):
        super(Profile,self).__init__(*args,**kwargs)
        self._meta.pk = Auth(self.id) if self.id else None

def get_profile(request):
    key = "_auth_user_id"
    if key in request.session:
        profile = Profile.objects(id=request.session[key])
        if profile.count()==1:
            return profile[0]
    return None
