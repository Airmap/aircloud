# Use this script after the bug that set all date months to minutes,
# resulting in all January timestamps
# recommended to call from a bash script one year at a time as
# the full loop through a collection uses too much RAM
import sys
sys.path.append('../')
import pdb
from models import *
from mongoengine import *
from datetime import datetime
import time

connect('airdb')
count = 0
yr = int(sys.argv[1])
coll_sel = int(sys.argv[2])
collection = [EpaPm25,EpaPm10,EpaOzone][coll_sel]
print('Selected '+['PM25','PM10','Ozone'][coll_sel])
print("year: "+str(yr))
count = 0
start = datetime(yr,1,1)
end = datetime(yr+1,1,1)
for obj in collection.objects(date__gte = start, date__lte = end).no_cache():
    if obj.date.minute > 0:
        count+=1
        date = datetime(obj.date.year, obj.date.minute, obj.date.day)
        obj.date = date
        obj.save()
        del obj
        del date
    print("count: "+str(count))
