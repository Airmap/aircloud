#!/usr/bin/python3
import sys
sys.path.append('../')
import pdb
from models import *
from mongoengine import connect
connect('airdb')

ok_ids = set(EpaSite.objects().distinct('site_id'))

# pm25
dist_ids_25 = set(EpaPm25.objects().distinct('aqs_site_id'))
to_add = dist_ids_25 - ok_ids
descriptors = [EpaPm25.objects(aqs_site_id=asid).first() for asid in to_add]

# pm10
dist_ids_10 = set(EpaPm10.objects().distinct('aqs_site_id'))
to_add2 = dist_ids_10-dist_ids_25-ok_ids
descriptors += [EpaPm10.objects(aqs_site_id=asid).first() for asid in to_add2]

# ozone
dist_ids_oz = set(EpaOzone.objects().distinct('aqs_site_id'))
to_add3 = dist_ids_oz-dist_ids_10-dist_ids_25-ok_ids
descriptors += [EpaOzone.objects(aqs_site_id=asid).first() for asid in to_add3]

print("Objects to update: "+str(len(descriptors)))
for obj in descriptors:
    id = obj.aqs_site_id
    EpaSite(site_id=id,
            name=obj.cbsa_name,
            lat=obj.site_latitude,
            lng=obj.site_longitude,
            pm25=id in dist_ids_25,
            pm10=id in dist_ids_10,
            ozone=id in dist_ids_oz).save()
