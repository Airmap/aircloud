google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(initTrendLineChart);

function initDataTables() {
      data_25 = new google.visualization.DataTable();
      data_25.addColumn('datetime', 'X');
      data_25.addColumn('number', 'PM 2.5 (ug/m3 LC)');
      data_25.addColumn('number', 'Moving Average');
      data_10 = new google.visualization.DataTable();
      data_10.addColumn('datetime', 'X');
      data_10.addColumn('number', 'PM 10 (ug/m3 SC)');
      data_10.addColumn('number', 'Moving Average');
      data_o3 = new google.visualization.DataTable();
      data_o3.addColumn('datetime', 'X');
      data_o3.addColumn('number', 'Ozone (ppm)');
      data_o3.addColumn('number', 'Moving Average');
}

function drawPm25(){
    $("#line_chart_pm25").html('');
    chart_25 = new google.visualization.ScatterChart(document.getElementById('line_chart_pm25'));
    chart_25.draw(data_25, options);
}

function drawPm10(){
    $("#line_chart_pm10").html('');
    chart_10 = new google.visualization.ScatterChart(document.getElementById('line_chart_pm10'));
    chart_10.draw(data_10, options);
}

function drawOzone(){
    $("#line_chart_ozone").html('');
    chart_o3 = new google.visualization.ScatterChart(document.getElementById('line_chart_ozone'));
    chart_o3.draw(data_o3, options);
}

function drawCharts() {
    drawPm25();
    drawPm10();
    drawOzone();
}

function initTrendLineChart() {
    $('#charts-container').hide()
    initDataTables();
    options = {
      hAxis: {
        title: 'Date',
        logScale: false
      },
      vAxis: {
        title: 'Concentration',
        logScale: false
      },
      colors: ['#a52714', '#097138'],
      series: {
        1: {
          lineWidth: 1,
          pointSize: 1
        }
      }
    };
    setLoaders();
}

var setLoaders = function(){
    loaderHtml = '<div class="loader"></div>'
    $('#line_chart_pm25').html(loaderHtml);
    $('#line_chart_pm10').html(loaderHtml);
    $('#line_chart_ozone').html(loaderHtml);
}

var pm25ChartSuccess = function(results){
    $('#charts-container').show()
    $('#pm25-cont').show();
    var points = results['data'];
    var n = 15;
    if (points.length > 0){
        var sum = 0;
        for (var i = 0; i < points.length; i++){
            //moving average line
           var divisor = n;
           if (divisor > i){
                divisor=i+1;
            } else{
                sum=sum-points[i-n+1][1];
            }
           sum = sum + points[i][1]
           points[i].push(sum/divisor);
           points[i][0]= new Date(points[i][0]);
        }
        data_25.addRows(points);
        drawPm25();
    } else {
        $('#pm25-cont').hide();
    }
}

var pm10ChartSuccess = function(results){
    $('#charts-container').show()
    $('#pm10-cont').show();
    points = results['data'];
    var n = 15;
    if (points.length > 0){
        var sum = 0;
        for (var i = 0; i < points.length; i++){
            //moving average line
           var divisor = n;
           if (divisor > i){
                divisor=i+1;
            } else{
                sum=sum-points[i-n+1][1];
            }
           sum = sum + points[i][1]
           points[i].push(sum/divisor);
           points[i][0]= new Date(points[i][0]);
        }

        data_10.addRows(points);
        drawPm10();
    } else {
        $('#pm10-cont').hide()
    }
}
var o3ChartSuccess = function(results){
    $('#charts-container').show()
    $('#o3-cont').show();
    points = results['data'];
    var n = 15;
    if (points.length > 0){
        var sum = 0;
        for (var i = 0; i < points.length; i++){
            //moving average line
           var divisor = n;
           if (divisor > i){
                divisor=i+1;
            } else{
                sum=sum-points[i-n+1][1];
            }
           sum = sum + points[i][1]
           points[i].push(sum/divisor);
           points[i][0]= new Date(points[i][0]);
        }
        data_o3.addRows(points);
        drawOzone();
    } else {
        $('#o3-cont').hide()
    }
}
var populateCharts = function(info,marker){
    setLoaders();
    var info_html = info.content;
    // DATES
    start_dt = $("#dt-start-txt").val().slice(0,10);
    end_dt = $("#dt-end-txt").val().slice(0,10);
    initDataTables();
    if (info_html.includes('PM 2.5')){
        pm25_query = {'site':marker.id,'poll':'pm25','start':start_dt,'end':end_dt};
        $.get('get_conc_vs_time/',pm25_query,pm25ChartSuccess);
    } else { $('#pm25-cont').hide(); }
    if (info_html.includes('PM 10')){
        pm10_query = {'site':marker.id,'poll':'pm10','start':start_dt,'end':end_dt};
        $.get('get_conc_vs_time/',pm10_query,pm10ChartSuccess);
    } else { $('#pm10-cont').hide(); }
    if (info_html.includes('Ozone')){
        o3_query =   {'site':marker.id,'poll':'ozone','start':start_dt,'end':end_dt};
        $.get('get_conc_vs_time/',o3_query,o3ChartSuccess);
    } else { $('#o3-cont').hide(); }
}

// air device csv
$('#air-data-csv').click(function(e){
    e.preventDefault();
    start_dt = $("#dt-start-txt").val().slice(0,10);
    end_dt = $("#dt-end-txt").val().slice(0,10);
    var csvUrl = 'create_csv/?source=airdev&'+jQuery.param({'start':start_dt,'end':end_dt});
    window.location.href = csvUrl;
});

$('#o3-csv').click(function(e){
    e.preventDefault();
    var csvUrl = 'create_csv/?source=epa&'+jQuery.param(o3_query);
    window.location.href = csvUrl;
});
$('#pm25-csv').click(function(e){
    e.preventDefault();
    var csvUrl = 'create_csv/?source=epa&'+jQuery.param(pm25_query);
    window.location.href = csvUrl;
});
$('#pm10-csv').click(function(e){
    e.preventDefault();
    var csvUrl = 'create_csv/?source=epa&'+jQuery.param(pm10_query);
    window.location.href = csvUrl;
});
