var loginSuccess = function(data) {
    console.log(data);
    if ('Success!' == data){
        $('#success-alert').html('<strong>Success!</strong> You are now logged in.');
        $('#success-alert').show();
        $('#info-alert').hide();
        $('#danger-alert').hide();
        window.location.reload(true);
    }
    else if ('User does not exist!' == data){
        $('#info-alert').html('<strong>Alert!</strong> '+data);
        $('#success-alert').hide();
        $('#danger-alert').hide();
        $('#info-alert').show();
    }
    else{
        $('#danger-alert').html('<strong>Oops!</strong> '+data);
        $('#success-alert').hide();
        $('#info-alert').hide();
        $('#danger-alert').show();
    }
}
var login = function(){
    var query = { 'uname': $('#username').val(),
                  'pwd' : $('#password').val()
    };
    var results = $.get("/login",query,loginSuccess).fail(function() {
        alert( "Error fetching!" );
    });
}

var createSuccess = function(data) {
    console.log(data)
    if (data == 'Username Created!'){
        $('#success-alert').html('<strong>Success!</strong> '+data);
        $('#success-alert').show();
        $('#info-alert').hide();
        $('#danger-alert').hide();
        login();
    }
    else if ('Username in Use!' == data){
        $('#info-alert').html('<strong>Uh-oh!</strong> '+data);
        $('#success-alert').hide();
        $('#danger-alert').hide();
        $('#info-alert').show();
    }
    else {
        $('#danger-alert').html('<strong>Oops!</strong> '+data+ ' Please try again.');
        $('#success-alert').hide();
        $('#info-alert').hide();
        $('#danger-alert').show();
    }
}
var createNewUser = function(){
    var query = { 'uname': $('#username').val(),
                  'pwd' : $('#password').val(),
                  'first' : $('#first').val(),
                  'last' : $('#last').val(),
                  'email' : $('#email').val()
    };
    var results = $.get("../create",query,createSuccess).fail(function() {
        alert( "Error fetching!" );
    })
}

// register user button
var regSwitchSuccess = function(data) {
    $('#login-form').html(data);
    $('#register-link').hide()
}
$('#regbutton').click(function(){
    var results = $.get("../reg_switch",{},regSwitchSuccess).fail(function() {
        alert( "Error!" );
    })
    return false;
});

// device button
var addDevSuccess = function(data) {
    console.log(data);
    if ('Success!' == data){
        window.location.reload(true);
    }
}
var addDevice = function(){
    var query = { 'id': $('#dev-id').val() };
    var results = $.get("/add_device",query,addDevSuccess).fail(function() {
        alert( "Error fetching!" );
    });
}

// api key button
// TODO handle Success/Failure
var apiKeySuccess = function(data) {
    console.log(data);
    if ('Success!' == data){
        window.location.reload(true);
    }
}
var regApiKey = function(){
    var query = { 'key': $('#api-key').val() };
    var results = $.get("/reg_api_key",query,addDevSuccess).fail(function() {
        alert( "Error fetching!" );
    });
}
