var map = null;
var hexgrid = [];
var dataPoints = [];
var epaMapData = {};
epaMapData.mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(42, -78),
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    },
    navigationControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
}
var ne = new google.maps.LatLng(43.26688772071543, -76.07264404296876);
var sw = new google.maps.LatLng(40.707378337910065, -81.52735595703126);


updateEpaMap = function() {
    console.log('sending GET request for new EPA map data');
    bounds = map.getBounds();
    
    // keep track globally of the map bounds for when we redraw the map
    ne = bounds.getNorthEast();
    sw = bounds.getSouthWest();

    // get request query parameters. These may actually be the same as the ne and sw just defined
    lng1 = bounds.b.b
    lng2 = bounds.b.f
    lat1 = bounds.f.b
    lat2 = bounds.f.f
    query['lat1'] = lat1
    query['lat2'] = lat2
    query['lng1'] = lng1
    query['lng2'] = lng2
    query['zoom'] = map.getZoom()
    $.get('get_epa_points/',query,epaPointsSuccess).fail(function() {
        alert('Error processing GET request for epa map data');
    });
}

var epaPointsSuccess = function(pointsJson){
    console.log(pointsJson);
    epaMapData = pointsJson;
    initMap()
}

function initMap() {
    map = new google.maps.Map(document.getElementById("map"),
                              epaMapData.mapOptions);
  
    createHexGrid();
  
    getPoints();
  
  
    // redraw colors based on number of points in hex tile
    for (var i = 0; i < hexgrid.length; i++) {
        if (hexgrid[i].contains == 0 || typeof hexgrid[i].contains == 'undefined') {
            hexgrid[i].setVisible(false);
        } else if (hexgrid[i].contains > 0 && hexgrid[i].contains < 2) {
            hexgrid[i].setOptions({fillColor:"#00ff00"});
        } else if (hexgrid[i].contains >= 2 && hexgrid[i].contains < 3) {
            hexgrid[i].setOptions({fillColor:"#ffff00"});
        } else if (hexgrid[i].contains >= 3) {
            hexgrid[i].setOptions({fillColor:"#ff0000"});
        }
    }
}

function getPoints() {
  var bounds = new google.maps.LatLngBounds();
  // Seed our dataset with random locations
  for (var i = 0; i < hexgrid.length; i++) {
    var hexbounds = new google.maps.LatLngBounds();
    for (var j = 0; j < hexgrid[i].getPath().getLength(); j++) {
      bounds.extend(hexgrid[i].getPath().getAt(j));
      hexbounds.extend(hexgrid[i].getPath().getAt(j));
    }
    hexgrid[i].bounds = hexbounds;
  }
  var span = bounds.toSpan();
  var locations = [];
  for (pointCount = 0; pointCount < 500; pointCount++) {
    place = new google.maps.LatLng(Math.random() * span.lat() + bounds.getSouthWest().lat(), Math.random() * span.lng() + bounds.getSouthWest().lng());
    bounds.extend(place);
    locations.push(place);
    var mark = new google.maps.Marker({
      map: map,
      position: place
    });
    // bin points in hexgrid
    for (var i = 0; i < hexgrid.length; i++) {
      if (google.maps.geometry.poly.containsLocation(place, hexgrid[i])) {
        if (!hexgrid[i].contains) {
          hexgrid[i].contains = 0;
        }
        hexgrid[i].contains++
      }
    }
  }
}

/*
 * Function to create hexbin overlay given values for the center coordinate.
 *
 * zoom is the maps API zoom value that we'll use to determine how many hexagons needed and size and stuff
 *
 * This may change later to not use hexagons since chris doesn't like hexagons i think a hexagon killed his mother
 * 
 * Only hexagons with actual data points in them will be visible.
 * This is how we're gonna do this unless it turns out Google has a problem with too many hexagons
 *
 * Hexagon color will also be set in this function based on average population values in them.
 *
 * It's probably bad to do all of this clientside.
 *
 */

function createHexGrid() {
    var mapWidth = google.maps.geometry.spherical.computeDistanceBetween(ne, 
                new google.maps.LatLng(ne.lat(), sw.lng()));
    var mapHeight = google.maps.geometry.spherical.computeDistanceBetween(sw, 
                new google.maps.LatLng(sw.lat(), ne.lng()));

    var hexagonsInRow = 20;                         // For now, let's do this so that there's 20 hexagons per row
    var hexRadius = Math.floor(mapWidth / hexagonsInRow);           // Yeah I think this makes sense to do
    var hexagonsInColumn = Math.ceil(mapHeight / hexRadius);

    // Let's just make the first hexagon center the northwest corner
    var startHexCenterPoint = new google.maps.LatLng(ne.lat(), sw.lng());

    // Draw hexagons fucking everywhere
    for (var i = 0; i < hexagonsInColumn; i++) {
        var currentHexCenter = startHexCenterPoint;
        
        for (var j = 0; j < hexagonsInRow; j++) {
            // Hexagons are drawn using Mike Williams' eshapes.js that have been ported to google maps v3
            // Documentation can be found at: econym.org.uk/gmap/eshapes.htm
            //
            // RegularPoly(center, radius(m), vertices, rotation, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity)
            var hex = google.maps.Polygon.RegularPoly(currentHexCenter,
                                                      hexRadius,            // Boy, there's a lot of variables with "hex" in the name, gonna be a bitch to change
                                                      6, 
                                                      90, 
                                                      "#000000", 
                                                      0,                    // no border around the shapes
                                                      1, 
                                                      "#00ff00",            
                                                      0.5);                   
            hex.setMap(map);

            hexgrid.push(hex);                  // keep track of our thing

            currentHexCenter = EOffsetBearing(currentHexCenter, 
                                        hexRadius * 2 * Math.cos(Math.PI / 6),       // Fucking magic
                                        90);
        }

        // Do that hexagon-y thing where hexagons are offset to fit kinda nicely together
        // You know what the fuck I mean
        if (i % 2) {
            startHexCenterPoint = EOffsetBearing(startHexCenterPoint, hexRadius * 2 * Math.cos(Math.PI/6), 150);
        } else {
            startHexCenterPoint = EOffsetBearing(startHexCenterPoint, hexRadius * 2 * Math.cos(Math.PI/6), 210);
        }
    }
}

google.maps.event.addDomListener(window, 'load', initMap);

// EShapes.js
//
// Based on an idea, and some lines of code, by "thetoy" 
//
//   This Javascript is provided by Mike Williams
//   Community Church Javascript Team
//   http://www.bisphamchurch.org.uk/   
//   http://econym.org.uk/gmap/
//
//   This work is licenced under a Creative Commons Licence
//   http://creativecommons.org/licenses/by/2.0/uk/
//
// Version 0.0 04/Apr/2008 Not quite finished yet
// Version 1.0 10/Apr/2008 Initial release
// Version 3.0 12/Oct/2011 Ported to v3 by Lawrence Ross

google.maps.Polygon.Shape = function(point, r1, r2, r3, r4, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt) {
    var rot = -rotation * Math.PI / 180;
    var points = [];
    var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
    var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
    var step = (360 / vertexCount) || 10;

    var flop = -1;
    if (tilt) {
        var I1 = 180 / vertexCount;
    } else {
        var I1 = 0;
    }
    for (var i = I1; i <= 360.001 + I1; i += step) {
        var r1a = flop ? r1 : r3;
        var r2a = flop ? r2 : r4;
        flop = -1 - flop;
        var y = r1a * Math.cos(i * Math.PI / 180);
        var x = r2a * Math.sin(i * Math.PI / 180);
        var lng = (x * Math.cos(rot) - y * Math.sin(rot)) / lngConv;
        var lat = (y * Math.cos(rot) + x * Math.sin(rot)) / latConv;

        points.push(new google.maps.LatLng(point.lat() + lat, point.lng() + lng));
    }
    return (new google.maps.Polygon({
        paths: points,
        strokeColor: strokeColour,
        strokeWeight: strokeWeight,
        strokeOpacity: Strokepacity,
        fillColor: fillColour,
        fillOpacity: fillOpacity
    }))
}

google.maps.Polygon.RegularPoly = function(point, radius, vertexCount, rotation, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts) {
    rotation = rotation || 0;
    var tilt = !(vertexCount & 1);
    return google.maps.Polygon.Shape(point, radius, radius, radius, radius, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt)
}

function EOffsetBearing(point, dist, bearing) {
    var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
    var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
    var lat = dist * Math.cos(bearing * Math.PI / 180) / latConv;
    var lng = dist * Math.sin(bearing * Math.PI / 180) / lngConv;
    return new google.maps.LatLng(point.lat() + lat, point.lng() + lng)
}
