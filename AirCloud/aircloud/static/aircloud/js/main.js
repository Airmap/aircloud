$(document).ready(function(){
    // default map view, centered on Boston
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {lat: 42.339887, lng: -71.089940},
        mapPoints: [],
        minZoom: 2,
    });

    var timeNow = new Date();
    var oneDayAgo = new Date();
    oneDayAgo.setDate(oneDayAgo.getDate() - 1);

    $('#dt-start').datetimepicker({
        defaultDate: oneDayAgo
    });
    $('#dt-end').datetimepicker({
        defaultDate: timeNow
    });
});

// TODO: Make a picker for this value in the UI
var timeIntervalInMilliseconds;

var map_query;                      // whoa really fucking cool, javascript
var geoMapQuery;

var buttonSubmitMapForm_onClick = function(e) {
    e.preventDefault();
    e.fromSubmit = true;
    // "Submit" does the same thing as "Animate, but with a single time range
    buttonAnimateHeatMap_onClick(e);
}

var buttonLiveData_onClick = function(e) {
    e.preventDefault();

    timeIntervalInMilliseconds = 2000;              // won't be used since live sends time duration as 1000 ms

    if(liveDataAnimationLoop == null) {

        $('#map-start-date').text("");
        $('#map-end-date').text("");

        // Disable the scrollwheel during the animation
        map.setOptions({
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false
        });

        $('html,body').animate({
            scrollTop: $('.map-cont').offset().top
        }, 200);

        map_query = getMapQueryParams(e.fromSubmit);

        clearHexGrid();

        var address = $("input[name='location']").val();
        // if location box isn't empty, transfer control to querying there
        if (address != "") {
            var geoUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + address;
            geoMapQuery = map_query;
            $.getJSON(geoUrl, geolocateQueryForLiveAnimationSuccess);
        }
        else {
            sendMapQueryForLiveAnimation(map_query);
        }
    } else {
        map.setOptions({
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false
        });
        window.clearInterval(liveDataAnimationLoop);
        liveDataAnimationLoop = null;
        $('#map-live-container').css("visibility", "hidden");
    }
}

var geolocateQueryForLiveAnimationSuccess = function(data) {
    // if we got results then update the lat and longs
    geolocatorData=json;
    if (geolocatorData.results.length > 0){
        // use bounds from first result if s && fromSubmituccessful
        loc = geolocatorData.results[0].geometry.bounds;
        geoMapQuery['lat1'] = loc.southwest.lat
        geoMapQuery['lat2'] = loc.northeast.lat
        geoMapQuery['lng1'] = loc.southwest.lng
        geoMapQuery['lng2'] = loc.northeast.lng
        // redraw the bounds of the map
        map.fitBounds(new google.maps.LatLngBounds(
            new google.maps.LatLng(geoMapQuery.lat1, geoMapQuery.lng1),
            new google.maps.LatLng(geoMapQuery.lat2, geoMapQuery.lng2)
            )
        );
    }
    else {
        $("#locator-alert").show()
    }
    sendMapQueryForLiveAnimation(geoMapQuery);
}

var liveDataAnimationLoop;                      // used by setInterval function to track the animation for live data

// continually queries for more map data
var sendMapQueryForLiveAnimation = function(map_query) {
    $('#map-start-date').css("visibility", "hidden");
    $('#map-end-date').css("visibility", "hidden");
    $('#map-live-container').css("visibility", "visible");

    liveDataAnimationLoop = window.setInterval(function() {
        var timeNow = new Date(Date.now());
        map_query.end = timeNow.toUTCString();;
        map_query.start = (new Date(timeNow.getTime() - 1)).toUTCString();

        $('#map-live-date').text(map_query.end.toLocaleString());

        $.get('populate_map/', map_query, updateMapForHexAnimationSuccess).fail(function(e) {
            map.setOptions({scrollwheel: true, navigationControl: true, mapTypeControl: true, scaleControl: true, draggable: true});
            window.clearInterval(liveDataAnimationLoop);
            alert('Error processing GET request for map data');
            console.log(e);
        });
    }, 1000);
}

var buttonAnimateHeatMap_onClick = function(e) {
    e.preventDefault();
    console.log(e);

    if (e.srcElement.name == "submit") {
        timeIntervalInMilliseconds = null;
    } else if (e.srcElement.name == "live") {
        timeIntervalInMilliseconds = 2000;              // won't be used since live sends time duration as 1000 ms
    } else if (e.srcElement.name == "animate") {
        timeIntervalInMilliseconds =
            ( (new Date($("#dt-end-txt").val())).getTime() - (new Date($("#dt-start-txt").val())).getTime() ) / 24;
    }

    $('#map-start-date').css("visibility", "visible");
    $('#map-end-date').css("visibility", "visible");

    console.log(timeIntervalInMilliseconds);

    $('#map-start-date').text("");
    $('#map-end-date').text("");

    // Disable the scrollwheel during the animation
    map.setOptions({scrollwheel: false, navigationControl: false, mapTypeControl: false, scaleControl: false, draggable: false});

    $('html,body').animate({
        scrollTop: $('.map-cont').offset().top
    },200);

    map_query = getMapQueryParams(e.fromSubmit);

    clearHexGrid();

    $("#air-data-csv").removeClass("disabled");
    var address = $("input[name='location']").val();
    // if location box isn't empty, transfer control to querying there
    if (address != ""){
        var geoUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + address;
        geoMapQuery = map_query;
        $.getJSON(geoUrl,geolocateQuerySuccess);
    }
    else {
        sendMapQuery(map_query);
    }
}

var sendMapQuery = function(map_query) {
    $('#map-loading-overlay').css("visibility", "visible");
    $('#map-loader').css("visibility", "visible");

    $.get('populate_map/', map_query, updateMapForHexAnimationSuccess).fail(function() {
        map.setOptions({scrollwheel: true, navigationControl: true, mapTypeControl: true, scaleControl: true, draggable: true});
        window.clearInterval(animationLoop);
        alert('Error processing GET request for map data');
    });

}

var geolocateQuerySuccess = function(json){
    // if we got results then update the lat and longs
    geolocatorData=json;
    if (geolocatorData.results.length > 0){
        // use bounds from first result if s && fromSubmituccessful
        loc = geolocatorData.results[0].geometry.bounds;
        geoMapQuery['lat1'] = loc.southwest.lat
        geoMapQuery['lat2'] = loc.northeast.lat
        geoMapQuery['lng1'] = loc.southwest.lng
        geoMapQuery['lng2'] = loc.northeast.lng
        // redraw the bounds of the map
        map.fitBounds(new google.maps.LatLngBounds(
            new google.maps.LatLng(geoMapQuery.lat1, geoMapQuery.lng1),
            new google.maps.LatLng(geoMapQuery.lat2, geoMapQuery.lng2)
            )
        );
    }
    else {
        $("#locator-alert").show()
    }
    sendMapQuery(geoMapQuery);
}

var getMapQueryParams = function(fromSubmit) {
    $("#locator-alert").hide()
    map_query = {}

    // POLLUTANT TYPE
    // get radio val
    var pollType = $("input[name='dataSource']:checked").val();
    pollType = pollType ? pollType : 'ozone';
    map_query['poll']=pollType;

    // DATES
    var start_dt;
    var end_dt;

    if ($("#dt-start-txt").val()) start_dt = (new Date($("#dt-start-txt").val())).toUTCString();
    if ($("#dt-end-txt").val()) end_dt = (new Date($("#dt-end-txt").val())).toUTCString();

    if (start_dt) map_query['start']=start_dt;
    if (end_dt) map_query['end']=end_dt;

    // SOURCES
    map_query['epa']=$('#epa-cb').is(':checked') && fromSubmit;
    map_query['ad']=$('#ad-cb').is(':checked');
    map_query['userad']=$('#user-ad-cb').is(':checked');

    // LOCATION
    // first try to get the values from the coordinate boxes
    lat1 = $("input[name='lat1']").val();
    lat2 = $("input[name='lat2']").val();
    lng1 = $("input[name='lng1']").val();
    lng2 = $("input[name='lng2']").val();
    // if there's nothing there, then try from the location box
    if (lat1 == "" || lat2 == "" || lng1 == "" || lng2 == ""){
        // if coord boxes are empty, use default map bounds
        bounds = map.getBounds()
        lng1 = bounds.b.b
        lng2 = bounds.b.f
        lat1 = bounds.f.b
        lat2 = bounds.f.f
    } else {
        // redraw the bounds of the map
        map.fitBounds(new google.maps.LatLngBounds(
                new google.maps.LatLng(lat1, lng1),
                new google.maps.LatLng(lat2, lng2)
            )
        );
    }
    map_query['lat1'] = lat1
    map_query['lat2'] = lat2
    map_query['lng1'] = lng1
    map_query['lng2'] = lng2
    map_query['zoom'] = map.getZoom()

    return map_query;
}

// heatMapData initialized array
var heatMapData = [];

var OZONE_MAX = 110; // ppm   TODO fix this to 56 ppb
var PM25_MAX = 150; // ug/m3
colors = [
"#00FF00", "#10FF00", "#20FF00",
"#30FF00", "#40FF00", "#50FF00",
"#60FF00", "#70FF00", "#80FF00",
"#90FF00", "#A0FF00", "#B0FF00",
"#C0FF00", "#D0FF00", "#E0FF00",
"#F0FF00", "#FFFF00", "#FFF000",
"#FFE000", "#FFD000", "#FFC000",
"#FFB000", "#FFA000", "#FF9000",
"#FF8000", "#FF7000", "#FF6000",
"#FF5000", "#FF4000", "#FF3000",
"#FF2000", "#FF1000", "#FF0000"]

// (re)initialize the map with the new data and markers
var markers = [];
function drawEpaMarkers(mapPoints) {
    for (var i = 0; i < markers.length; i++){
        markers[i].setMap(null);
    }
    markers = [];
    infoWindows = Array.from(mapPoints, x => new google.maps.InfoWindow({
        content: x.description
    }));
    markers = Array.from(mapPoints, x => new google.maps.Marker({
        position: {lat: x.lat, lng: x.lng},
        map: map,
        id: x.id,
        title: x.title ? x.title : '(' + x.lat + ', ' + x.lng + ')',
        visible: $('#epa-cb').is(':checked')
    }));
    for (var i = 0; i < mapPoints.length; i++) {
        markers[i].addListener('click', (function(i) {
            return function() {
                infoWindows[i].open(map, markers[i]);
                populateCharts(infoWindows[i],markers[i]);
                $('html,body').animate({
                    scrollTop: $('#metrics-head').offset().top
                },2500);
            }
        })(i));
    }
}

// get array of dicts for heatmap points with weight
function getPoints(data) {
    dicts = [];
    for (var i=0; i < data.ad_data.length; i++){
        x = data.ad_data[i];
        dicts.push({location:new google.maps.LatLng(x.lat, x.lng),weight:x.conc,date:new Date(x.date)})
    }
    return dicts;
}

var animationLoop;                  // global variable to keep track of the animation drawn using setInterval

var hexgrid = [];


var updateMapForHexAnimationSuccess = function(pointsJson) {

    createHexGrid();

    var timeRanges = [];
    var timeRangeStart = new Date(map_query.start);
    var timeRangeEnd = new Date(timeRangeStart);
    var endBound = new Date(map_query.end);

    drawEpaMarkers(pointsJson.mapPoints)
    var heatMapData = getPoints(pointsJson);

    // create time ranges
    if (timeIntervalInMilliseconds != null) {
        while (timeRangeEnd < endBound.getTime()) {
            timeRangeEnd = timeRangeStart.getTime() + timeIntervalInMilliseconds < endBound
                ? new Date(timeRangeStart.getTime() + timeIntervalInMilliseconds)
                : endBound;
            timeRanges.push({start: timeRangeStart, end: timeRangeEnd, heatMapData: [], hexbins: []})
            timeRangeStart = new Date(timeRangeEnd);
        }
    } else {                    // create a single time range spanning start to end if not defined
        timeRanges.push({start: timeRangeStart, end: endBound, heatMapData: [], hexbins: []})
    }

    // put points in the correct time range
    for (var i = 0; i < heatMapData.length; i++) {
        for (var j = 0; j < timeRanges.length; j++) {
            if (heatMapData[i].date.getTime() >= timeRanges[j].start.getTime() && heatMapData[i].date.getTime() < timeRanges[j].end.getTime()) {
                timeRanges[j].heatMapData.push(heatMapData[i]);
                break;
            }
        }
    }

    // determine values relevant to the animation for each time range
    for (var i = 0; i < timeRanges.length; i++) {
        // for each timeRange, create an empty array of hexbin values
        for (var j = 0; j < hexgrid.length; j++) {
            timeRanges[i].hexbins[j] = {points: [], color:""};
        }

        // bin points into hexagons
        for (var j = 0; j < timeRanges[i].heatMapData.length; j++) {
            for (var k = 0; k < hexgrid.length; k++) {
                if (google.maps.geometry.poly.containsLocation(timeRanges[i].heatMapData[j].location, hexgrid[k])) {
                    timeRanges[i].hexbins[k].points.push(timeRanges[i].heatMapData[j]);
                    break;
                }
            }
        }

        var pollutantType = pointsJson['poll'];
        var max = pollutantType == 'ozone' ? OZONE_MAX : PM25_MAX;
        // calculate colors for each hexbin based on the average of points' weights
        for (var j = 0; j < timeRanges[i].hexbins.length; j++) {
            if (timeRanges[i].hexbins[j].points.length > 0) {
                var average = 0;
                for (var k = 0; k < timeRanges[i].hexbins[j].points.length; k++) {
                    average += timeRanges[i].hexbins[j].points[k].weight;
                }
                average = average / timeRanges[i].hexbins[j].points.length;
                var grad_sel = Math.min(Math.round(average * (colors.length - 1) / max), colors.length - 1);
                timeRanges[i].hexbins[j].color = colors[grad_sel];
            }
        }

    }

    $('#map-loader').css("visibility", "hidden");
    $('#map-loading-overlay').css("visibility", "hidden");

    drawHexAnimationFrame(timeRanges, 0);           // call drawHexAnimationFrame() explicitly to avoid delay the first time
    var i = 1;
    animationLoop = window.setInterval(function () {
        drawHexAnimationFrame(timeRanges, i);
        i++;
    }, 500);
}

var drawHexAnimationFrame = function(timeRanges, i) {
    if (i < timeRanges.length) {
        for (var j = 0; j < hexgrid.length; j++) {
            if (timeRanges[i].hexbins[j].color != "") {
                hexgrid[j].setOptions({fillColor:timeRanges[i].hexbins[j].color});
                hexgrid[j].setVisible(true);
            } else hexgrid[j].setVisible(false);
        }
        $('#map-start-date').text(timeRanges[i].start.toLocaleString());
        $('#map-end-date').text(timeRanges[i].end.toLocaleString());

    } else {
        map.setOptions({scrollwheel: true, navigationControl: true, mapTypeControl: true, scaleControl: true, draggable: true});
        window.clearInterval(animationLoop);
    }

}

/*
 * Clean up any existing hexgrid
 */
function clearHexGrid() {
    for (var i = 0; i < hexgrid.length; i++) {
        hexgrid[i].setMap(null);
    }
    hexgrid = [];
}

/*
 * Function to create hexbin overlay given values for the center coordinate.
 *
 * zoom is the maps API zoom value that we'll use to determine how many hexagons needed and size and stuff
 *
 * This may change later to not use hexagons since chris doesn't like hexagons i think a hexagon killed his mother
 *
 * Only hexagons with actual data points in them will be visible.
 * This is how we're gonna do this unless it turns out Google has a problem with too many hexagons
 *
 * Hexagon color will also be set in this function based on average population values in them.
 *
 * It's probably bad to do all of this clientside.
 *
 */

function createHexGrid() {
    clearHexGrid();

    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
    var mapWidth = google.maps.geometry.spherical.computeDistanceBetween(ne,
                new google.maps.LatLng(ne.lat(), sw.lng()));
    var mapHeight = google.maps.geometry.spherical.computeDistanceBetween(sw,
                new google.maps.LatLng(sw.lat(), ne.lng()));

    var hexagonsInRow = 50;                         // For now, let's do this so that there's 20 hexagons per row
    var hexRadius = Math.floor(mapWidth / hexagonsInRow);           // Yeah I think this makes sense to do
    var hexagonsInColumn = Math.ceil(mapHeight / hexRadius);

    // Let's just make the first hexagon center the northwest corner
    var startHexCenterPoint = new google.maps.LatLng(ne.lat(), sw.lng());

    // Draw hexagons fucking everywhere
    for (var i = 0; i < hexagonsInColumn; i++) {
        var currentHexCenter = startHexCenterPoint;

        for (var j = 0; j < hexagonsInRow; j++) {
            // Hexagons are drawn using Mike Williams' eshapes.js that have been ported to google maps v3
            // Documentation can be found at: econym.org.uk/gmap/eshapes.htm
            //
            // RegularPoly(center, radius(m), vertices, rotation, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity)
            var hex = google.maps.Polygon.RegularPoly(currentHexCenter,
                                                      hexRadius,            // Boy, there's a lot of variables with "hex" in the name, gonna be a bitch to change
                                                      6,
                                                      90,
                                                      "#000000",
                                                      0,                    // no border around the shapes
                                                      1,
                                                      "#00ff00",
                                                      0.5);
            hex.setVisible(false);
            hex.setMap(map);

            // Datapoints contained by the Hexagon. To be populated later
            hex.points = [];

            hexgrid.push(hex);                  // keep track of our thing

            currentHexCenter = EOffsetBearing(currentHexCenter,
                                        hexRadius * 2 * Math.cos(Math.PI / 6),       // Fucking magic
                                        90);
        }

        // Do that hexagon-y thing where hexagons are offset to fit kinda nicely together
        // You know what the fuck I mean
        if (i % 2) {
            startHexCenterPoint = EOffsetBearing(startHexCenterPoint, hexRadius * 2 * Math.cos(Math.PI/6), 150);
        } else {
            startHexCenterPoint = EOffsetBearing(startHexCenterPoint, hexRadius * 2 * Math.cos(Math.PI/6), 210);
        }
    }
}

// EShapes.js
//
// Based on an idea, and some lines of code, by "thetoy"
//
//   This Javascript is provided by Mike Williams
//   Community Church Javascript Team
//   http://www.bisphamchurch.org.uk/
//   http://econym.org.uk/gmap/
//
//   This work is licenced under a Creative Commons Licence
//   http://creativecommons.org/licenses/by/2.0/uk/
//
// Version 0.0 04/Apr/2008 Not quite finished yet
// Version 1.0 10/Apr/2008 Initial release
// Version 3.0 12/Oct/2011 Ported to v3 by Lawrence Ross

google.maps.Polygon.Shape = function(point, r1, r2, r3, r4, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt) {
    var rot = -rotation * Math.PI / 180;
    var points = [];
    var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
    var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
    var step = (360 / vertexCount) || 10;

    var flop = -1;
    if (tilt) {
        var I1 = 180 / vertexCount;
    } else {
        var I1 = 0;
    }
    for (var i = I1; i <= 360.001 + I1; i += step) {
        var r1a = flop ? r1 : r3;
        var r2a = flop ? r2 : r4;
        flop = -1 - flop;
        var y = r1a * Math.cos(i * Math.PI / 180);
        var x = r2a * Math.sin(i * Math.PI / 180);
        var lng = (x * Math.cos(rot) - y * Math.sin(rot)) / lngConv;
        var lat = (y * Math.cos(rot) + x * Math.sin(rot)) / latConv;

        points.push(new google.maps.LatLng(point.lat() + lat, point.lng() + lng));
    }
    return (new google.maps.Polygon({
        paths: points,
        strokeColor: strokeColour,
        strokeWeight: strokeWeight,
        strokeOpacity: Strokepacity,
        fillColor: fillColour,
        fillOpacity: fillOpacity
    }))
}

google.maps.Polygon.RegularPoly = function(point, radius, vertexCount, rotation, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts) {
    rotation = rotation || 0;
    var tilt = !(vertexCount & 1);
    return google.maps.Polygon.Shape(point, radius, radius, radius, radius, rotation, vertexCount, strokeColour, strokeWeight, Strokepacity, fillColour, fillOpacity, opts, tilt)
}

function EOffsetBearing(point, dist, bearing) {
    var latConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat() + 0.1, point.lng())) * 10;
    var lngConv = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(point.lat(), point.lng() + 0.1)) * 10;
    var lat = dist * Math.cos(bearing * Math.PI / 180) / latConv;
    var lng = dist * Math.sin(bearing * Math.PI / 180) / lngConv;
    return new google.maps.LatLng(point.lat() + lat, point.lng() + lng)
}

function improveSite() {
    document.getElementsByTagName('body')[0].innerHTML = ('<marquee scrollamount="250">' + document.getElementsByTagName('body')[0].innerHTML + '</marquee>')
}

