'''

A script for scraping data off of the EPA's airdata/ API

'''

import pdb
import sys
from mongoengine import *
import models
import urllib.request
import csv
from models import *
import time
from datetime import datetime
template= 'https://www3.epa.gov/cgi-bin/broker?_service=data&_debug=0&_program=dataprog.ad_data_daily.sas&querytext=&areaname=&areacontacts=&areasearchurl=&typeofsearch=epa&result_template=2col.ftl&poll={0}&year={1}&state={2:02d}&cbsa=-1&county=-1&site=-1'
OZONE = '44201'
PM2_5 = '88101%27%2C%2788502'
PM10 = '81102'
ALL = [PM2_5,PM10,OZONE]
YEAR_RANGE = range(2016,2017)

# function for processing pm2.5, pm10, or ozone data from a csv
# would look cleaner to separate but they're just so similar
def save_to_db(reader, headers, poll_type):
    # Daily Mean PM10 Concentration, Daily Mean PM2.5 Concentration,
    # Daily Max 8-hour Ozone Concentration > concentration
    headers[3] = 'concentration'
    for i in range(len(headers)):
        headers[i] = headers[i].lower()
    # The data model property names now align directly with the headers
    # in the csv so that setattr can be called
    if poll_type == PM10:
        db_type = EpaPm10
    elif poll_type == PM2_5:
        db_type = EpaPm25
    elif poll_type == OZONE:
        db_type = EpaOzone
    else:
        raise TypeError('Unrecognized pollutant type!')

    for line in reader:

        db_obj = db_type()
        try:
            for i in range(len(headers)):
                # cast the field to the right type
                if isinstance(getattr(db_type,headers[i]),StringField):
                    setattr(db_obj,headers[i],line[i])
                elif isinstance(getattr(db_type,headers[i]), DateTimeField):
                    setattr(db_obj,headers[i],datetime.strptime(line[i],"%m/%d/%Y"))
                elif isinstance(getattr(db_type,headers[i]),IntField):
                    # some are coming in empty so make sure that it is a populated field
                    # else the cast will fail and break
                    if line[i]:
                        setattr(db_obj,headers[i],int(line[i]))
                elif isinstance(getattr(db_type,headers[i]),FloatField):
                    # Same ^
                    if line[i]:
                        setattr(db_obj,headers[i],float(line[i]))
        except:
            # Data errors can just be ignored
            continue
        # persist to the database
        db_obj.save()

def scrape(pollutants):
    connect('airdb')
    for poll in pollutants:
        for year in YEAR_RANGE:
            print('Year: %i'%year)
            for state in range(1,51):
                # make the query and post it
                req = template.format(poll,year,state)
                # get the ridiculous html back with the link
                response = urllib.request.urlopen(req)
                resp_html = str(response.read())
                if 'no values' in resp_html:
                    print('None found for state %i'%state)
                    continue
                elif 'error' in resp_html:
                    pdb.set_trace()
                else:
                    # and parse the link out
                    csv_link = resp_html.split('href="',1)[1].split('"',1)[0]
                # open the link and get the data object
                data = urllib.request.urlopen(csv_link)
                # then read the csv into a string
                data_csv = str(data.read())
                reader = csv.reader(data_csv.split("b\'",1)[1].split('\\n')[:-1],delimiter=',')
                headers = reader.__next__()
                save_to_db(reader, headers, poll)
                # The EPA guy told me to put a pause in but... it's probably chill
                # time.sleep(2)

if __name__=="__main__":
    if (len(sys.argv)<2):
        print ('Selection argument not provided!\nSelect:\
              \n0 for ALL\n1 for PM2_5\n2 for PM10\n3 for OZONE')
    else:
        sel = int(sys.argv[1])
        polls = [ALL,PM2_5,PM10,OZONE][sel]
        scrape(polls) if polls == ALL else scrape([polls])
