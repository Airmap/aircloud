'''
    The Django / MongoDB data models, bridged using mongoengine
'''

from mongoengine import *

class GPS(EmbeddedDocument):
    '''
        A subdocument for gps coordinates
    '''

    latitude = FloatField()
    longitude = FloatField()
    number_satellites = IntField()
    run_status = IntField()
    fix_status = IntField()
    unix_time = FloatField()
    altitude = FloatField()
    altitude_units = StringField()

class DeviceRecord(Document):
    device_id = IntField()
    # datetime object (e.g. datetime.datetime(2016, 9, 11, 19, 3,30,500000) for 7:03:30.5
    datetime = DateTimeField()
    gps = EmbeddedDocumentField(GPS)

    temperature = FloatField()
    temperature_units = StringField()
    humidity = IntField()
    humidity_units = StringField()
    pressure = FloatField()
    pressure_units = StringField()

    ozone_concentration = FloatField()
    ozone_units = StringField()
    pm25_concentration = FloatField()
    pm25_units = StringField()

class EpaPm10(Document):
    date = DateTimeField()
    aqs_site_id = IntField()
    poc = IntField()
    concentration = FloatField()
    units = StringField(max_length=20)
    daily_aqi_value = IntField()
    daily_obs_count = IntField()
    percent_complete = FloatField()
    aqs_parameter_code = IntField()
    aqs_parameter_desc = StringField()
    cbsa_code = IntField()
    cbsa_name = StringField()
    state_code = IntField()
    state = StringField()
    county_code = IntField()
    county = StringField()
    site_latitude = FloatField()
    site_longitude = FloatField()

class EpaPm25(Document):
    date = DateTimeField()
    aqs_site_id = IntField()
    poc = IntField()
    concentration = FloatField()
    units = StringField(max_length=20)
    daily_aqi_value = IntField()
    daily_obs_count = IntField()
    percent_complete = FloatField()
    aqs_parameter_code = IntField()
    aqs_parameter_desc = StringField()
    cbsa_code = IntField()
    cbsa_name = StringField()
    state_code = IntField()
    state = StringField()
    county_code = IntField()
    county = StringField()
    site_latitude = FloatField()
    site_longitude = FloatField()

class EpaOzone(Document):
    date = DateTimeField()
    aqs_site_id = IntField()
    poc = IntField()
    concentration = FloatField()
    units = StringField(max_length=20)
    daily_aqi_value = IntField()
    daily_obs_count = IntField()
    percent_complete = FloatField()
    aqs_parameter_code = IntField()
    aqs_parameter_desc = StringField()
    cbsa_code = IntField()
    cbsa_name = StringField()
    state_code = IntField()
    state = StringField()
    county_code = IntField()
    county = StringField()
    site_latitude = FloatField()
    site_longitude = FloatField()

class EpaSite(Document):
    name = StringField()
    site_id = IntField()
    lat = FloatField()
    lng = FloatField()
    pm25 = BooleanField()
    pm10 = BooleanField()
    ozone = BooleanField()

class DeviceKey(Document):
    key = StringField()

if __name__=="__main__":
    connect('airdb')
    import pdb
    pdb.set_trace()
    pass
