import sys
import pdb
from models import *
from mongoengine import *
from datetime import datetime, timedelta
import random
import time
connect('airdb')

BAD = [
((42.346151,-71.062097),(42.347544,-71.068737)),
((42.346151,-71.062097),(42.349950,-71.059827)),
]
MODERATE = [
((42.348285,-71.070777),(42.347695,-71.101405)),
((42.335323,-71.066399),(42.321371,-71.054429)),
]
GOOD = [
((42.345264,-71.070202),(42.343651,-71.064850)),
((42.345264,-71.070202),(42.332095,-71.094425)),
((42.343234,-71.065132),(42.329024,-71.082877)),
((42.343234,-71.065132),(42.334646,-71.088933)),
((42.346565,-71.080342),(42.334386,-71.104565)),
]

POINTS_PER_DAY = 48 # Make this at least 24
POINTS_PER_LINE = 10
DEVICE_ID = 11111111
DAY = datetime(2016,11,30,0,0,0)
O3_MAX = 110
PM25_MAX = 150
if __name__=="__main__":
    for i in range(POINTS_PER_DAY):
        time_dt = timedelta(hours=24*i/POINTS_PER_DAY)
        for range_tuple in GOOD+BAD+MODERATE:
            lat1,lng1 = range_tuple[0]
            lat2,lng2 = range_tuple[1]
            if lat1 > lat2:
                lat1,lat2 = lat2,lat1
            if lng1 > lng2:
                lng1,lng2 = lng2,lng1
            level = 'good'
            if range_tuple in MODERATE:
                level = 'moderate'
            elif range_tuple in BAD:
                level = 'bad'
            for div in range(1,POINTS_PER_LINE):
                lat = lat1+abs(lat2-lat1)/div
                lng = lng1+abs(lng2-lng1)/div

                if level == 'bad':
                    o3 = O3_MAX * (2+random.random())/3
                    pm = PM25_MAX * (2+random.random())/3
                elif level == 'moderate':
                    o3 = O3_MAX * (1+random.random())/3
                    pm = PM25_MAX * (1+random.random())/3
                else:
                    o3 = O3_MAX * (random.random())/3
                    pm = PM25_MAX * (random.random())/3

                gps = GPS(latitude = lat,
                          longitude = lng,
                          number_satellites=11,
                          run_status = 1,
                          fix_status = 1,
                          unix_time = time.time(),
                          altitude = random.randint(0,10),
                          altitude_units = 'm')
                rec = DeviceRecord(device_id = DEVICE_ID,
                                   datetime = DAY + time_dt,
                                   gps = gps,
                                   temperature = 22.0 + random.random()%0.8,
                                   temperature_units = 'C',
                                   humidity = 30+ random.randint(0,10),
                                   humidity_units = '%',
                                   pressure = 101000 + random.randint(0,1000),
                                   pressure_units = 'Pa',
                                   ozone_concentration = o3,
                                   ozone_units = 'ppb',
                                   pm25_concentration = pm,
                                   pm25_units = 'ug/m3')
                rec.save()
