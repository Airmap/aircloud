from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.log_in, name='login'),
    url(r'^logout/$', views.log_out, name='logout'),
    url(r'^add_device/$', views.add_device, name='add device'),
    url(r'^reg_api_key/$', views.update_api_key, name='update api key'),
    url(r'^reg_switch/$', views.new_user_form, name='get new user html form'),
    url(r'^about/$', views.about, name='about airmap'),
    url(r'^create/$', views.create_username, name='create'),
    url(r'^get_conc_vs_time/$', views.get_conc_vs_time, name='get_conc_vs_time'),
    url(r'^populate_map/$', views.populate_map, name='populate map'),
    url(r'^create_csv/$', views.create_csv, name="generate csv"),
    url(r'^post_device_data/$', views.post_device_data, name="AirDevice data handler"),
    url(r'^home2/$', views.home2, name='home2'),
]
