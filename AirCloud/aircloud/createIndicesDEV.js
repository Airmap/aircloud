use airdb
db.epa_site.createIndex({'gps__latitude':1})
db.epa_site.createIndex({'gps__longitude':1})
db.epa_pm25.createIndex({'datetime':1})
