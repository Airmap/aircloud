'''
    The Django views for handling routes
'''
import mongoengine

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from mongoengine.django.auth import User
from aircloud.models import *
from aircloud.user_models import *
from datetime import datetime, timedelta
from multiprocessing.pool import ThreadPool
import json,csv
SESSION_KEY = "_auth_user_id"

PM2_5 = 'pm25'
PM10 = 'pm10'
OZONE = 'ozone'

EPA = 'epa'
DEVICE = 'airdev'

########### MAIN PAGE ###########

# The main page of the AirMap site
def index(request):
    current_user = get_profile(request)
    context = {'username':current_user.username} if current_user else {}
    return render(request, 'aircloud/index.html', context)

# TODO destroy this url
def home2(request):
    current_user = get_profile(request)
    return render(request, 'aircloud/home2.html')


########### LOGIN ###########

# Return Messages #
SUCCESS = 'Success!'
FAILED_AUTH ='Failed to authenticate!'
USER_NOT_FOUND = 'User does not exist!'
CREATED = 'Username Created!'
IN_USE = 'Username in Use!'
LOGGED_OUT = 'Successfully logged out!'
BAD_REQUEST = 'Bad Request!'
FAILURE = 'Failure!'
NOT_VALID = 'Failed to Validate!'
###################

# Defines the view for the login page, which includes
# simple login form, registration form, and user profile
def log_in(request):
    name = request.GET.get('uname')
    pwd = request.GET.get('pwd')
    # If this is a simple login
    if name:
        try:
            # Try to authenticate
            user = authenticate(username=name,password=pwd)
            if user is not None:
                assert isinstance(user, mongoengine.django.auth.User)
                # set login expiration to an hour
                request.session.set_expiry(60 * 60 * 1)
                # This is a terrible bug baked into the framework that I'm hacking around
                fix_req_for_auth(request)
                login(request,user)
                user.number_logins += 1
                user.save()
                return HttpResponse(SUCCESS)
            # Let the user know that auth failed
            else: return HttpResponse(FAILED_AUTH)
        # Let the user know that the username doesnt exist
        except DoesNotExist:
            return HttpResponse(USER_NOT_FOUND)
    # if there's no user and password passed in, then this users probably already logged in
    else:
        # get their profile information
        current_user = get_profile(request)
        context = {'username':str(current_user.username),
                   'email':str(current_user.email),
                   'date' : current_user.date_joined,
                   'first':str(current_user.first_name)} if current_user else {}
        context['contrib'] = bool(current_user.is_contributor) if current_user else False
        context['devices'] = list(current_user.device_ids) if current_user else []
        # render their profile
        return render(request, 'aircloud/login.html', context)

# This doesn't work. There's a shite bug with mongoengine and django auth on Id number
def log_out(request):
    fix_req_for_auth(request)
    logout(request)
    return HttpResponse(LOGGED_OUT)

# bug fixer
def fix_req_for_auth(request):
    if SESSION_KEY in request.session:
        request.session[SESSION_KEY] = int(Auth(request.session[SESSION_KEY]))

# The simple registration form html
def new_user_form(request):
    return render(request, 'aircloud/newuser.html')

# Submission of the registration form
def create_username(request):
    try:
        # Get the user params for the django user
        name = request.GET.get('uname')
        pwd = request.GET.get('pwd')
        email = request.GET.get('email')
        first =  request.GET.get('first')
        last =  request.GET.get('last')
        profile = Profile.create_user(username=name,password=pwd,email=email)
        # Update the AirMap profile params
        profile.first_name = first
        profile.last_name = last
        profile.date_joined = datetime.now()
        profile.is_contributor = False
        profile.api_key = ''
        profile.device_ids = []
        profile.number_logins = 0
        profile.save()
        # Notify successful user creation
        return HttpResponse(CREATED)
    except mongoengine.errors.NotUniqueError:
        # notify the username already exists
        return HttpResponse(IN_USE)
    except mongoengine.errors.ValidationError as e:
        # Send the failure message
        #return HttpResponse(e.message)
        return HttpResponse(NOT_VALID)

# For registering devices to your username
def add_device(request):
    dev_id = request.GET.get('id')
    if dev_id:
        user = get_profile(request)
        if user.is_contributor and dev_id not in user.device_ids:
            # TODO format checks and maybe a central db device id check
            user.device_ids.append(dev_id)
            user.save()
            # user was updated to add device, return success message
            return HttpResponse(SUCCESS)
        # if user cannot add a device or already has, return failure message
        else: return HttpResponse(FAILURE)
    # if no ID return a bad request notifier
    return HttpResponse(BAD_REQUEST, status=400)

# Register an API key to a user
def update_api_key(request):
    key = request.GET.get('key')
    if key:
        user = get_profile(request)
        if key in [dev.key for dev in DeviceKey.objects()]:
            # mark user a contributer so they can add devices
            user.is_contributor = True
            user.api_key = key
            user.save()
            # Return success message
            return HttpResponse(SUCCESS)
    # return failure if bad request or key not in central key store
    return HttpResponse(FAILURE)


############ ABOUT PAGE ############

def about(request):
    current_user = get_profile(request)
    context = {'username':current_user.username} if current_user else {}
    return render(request, 'aircloud/about.html', context)



######### DATA INPUT ############


# Return Messages #
CORRUPT = 'Corrupt Data!'
UNREGISTERED = 'Unregistered device'
###################

# Get the http post from the AirDevices
# make it exempt from needing a cookie
@csrf_exempt
def post_device_data(request):
    # Get the message, decode it (throwing error if corrupt), and load into a dictionary
    try:
        data=request.read()
        req_data = json.loads(data.decode())
    except:
        return HttpResponse(CORRUPT, status=500)
    api_key = req_data.get('device_api_key')
    # Make sure this key matches registered devices
    if DeviceKey.objects(key__exists=api_key).count() > 0:
        # the data object should directly map to a DeviceRecord mongoengine object
        rec_dict = req_data.get('data')
        # because of the python2 to python3 switch, we'll need to clean the date
        if not isinstance(rec_dict['datetime']['$date'],datetime):
            rec_dict['datetime']=datetime.fromtimestamp(rec_dict['datetime']['$date']/1000 -5*60*60) # TODO address this EST hardcode

        DeviceRecord(**dict(rec_dict)).save()
        # if all went well, save it
        return HttpResponse(SUCCESS,status=200)
    # Notify this is not a registered device if it doesn't come with a valid key
    else: return HttpResponse(UNREGISTERED,status=400)





############ QUERYING ############

# Query helper for AirDevice data so this can be threaded
def query_ad_data(lat1,lat2,lng1,lng2,poll,start,end,user_devs = None):
    results = DeviceRecord.objects(gps__latitude__gte=lat1, gps__latitude__lte=lat2,
                    gps__longitude__gte=lng1, gps__longitude__lte=lng2,
                    datetime__gte=start,
                    datetime__lte=end,)
    if user_devs:
        results = results.filter(device_id__in = user_devs)
    if poll == 'ozone':
        ad_results = [{'lat':obj.gps.latitude,'lng':obj.gps.longitude,'conc':obj.ozone_concentration,'date':obj.datetime.isoformat()} for obj in results]
    elif poll == 'pm25':
        ad_results = [{'lat':obj.gps.latitude,'lng':obj.gps.longitude,'conc':obj.pm25_concentration,'date':obj.datetime.isoformat()} for obj in results]
    elif poll == 'pm10':
        ad_results = [{'lat':obj.gps.latitude,'lng':obj.gps.longitude,'conc':obj.pm10_concentration,'date':obj.datetime.isoformat()} for obj in results]
    return ad_results

# Main view of the map representing submission of the query
def populate_map(request):
    # get booleans for querying epa, device, and only the user's devices
    epa = request.GET.get('epa') == 'true'
    device = request.GET.get('ad') == 'true'
    user_dev_only = request.GET.get('userad') == 'true'
    try:
        user_devs = None
        if user_dev_only:
            user = get_profile(request)
            user_devs = user.device_ids
    except:
        pass # fook it dont want to handle this yet

    poll = request.GET.get('poll')

    # get start and end - DEFAULT IS TODAY TO A YEAR AGO
    sd = request.GET.get('start')
    ed = request.GET.get('end')
    end_date = datetime.strptime(ed, "%a, %d %b %Y %H:%M:%S %Z") if ed else datetime.now();
    start_date = datetime.strptime(sd, "%a, %d %b %Y %H:%M:%S %Z") if sd else end_date-timedelta(days=365)

    zoom = int(request.GET.get('zoom'))
    lat1 = request.GET.get('lat1')
    lat2 = request.GET.get('lat2')
    lng1 = request.GET.get('lng1')
    lng2 = request.GET.get('lng2')
    zoom = int(request.GET.get('zoom'))
    # Front end needs to assure it sends bounds
    if lat1 and lng1 and lat2 and lng2:
        lat1 = float(lat1)
        lat2 = float(lat2)
        lng1 = float(lng1)
        lng2 = float(lng2)
        # make sure we have a rectangle. swap if necessary
        if lat1 > lat2:
            lat1,lat2 = lat2,lat1
        if lng1 > lng2:
            lng1,lng2 = lng2,lng1
    else: return HttpResponse(BAD_REQUEST, status=400)

    # run the AD query in a separate thread
    if device:
        pool = ThreadPool(processes=1)
        async_res = pool.apply_async(query_ad_data,(lat1,lat2,lng1,lng2,
                                                    poll,
                                                    start_date,
                                                    end_date,
                                                    user_devs))

    map_points = []
    # run EPA query here
    if epa:
        sites = EpaSite.objects(lat__gte=lat1, lat__lte=lat2,
                                lng__gte=lng1, lng__lte=lng2)
        for site in sites:
            types = ['PM 2.5'] if site.pm25 else []
            types += ['PM 10'] if site.pm10 else []
            types += ['Ozone'] if site.ozone else []
            marker_desc = {'lat': site.lat,
                           'lng':site.lng,
                           'description':'<h4>Site Id: {}</h4>'.format(site.site_id)+
                                         '<h3>{}</h3>'.format(', '.join(types))+
                                         '<p>({}, {})</p>'.format(site.lat,site.lng)+
                                         '<p>{}</p>'.format(site.name),
                           'id':str(site.site_id)}
            map_points.append(marker_desc)
    zoom = zoom
    center = {'lat': (lat2+lat1)/2,'lng': (lng2+lng1)/2}
    response_dict = {'poll':poll,
                     'zoom': zoom,
                     'center': center,
                     'mapPoints': map_points}

    # get our AD query results
    if device:
        # TODO do a comparison check on time of this vs just calling the function to see what we're gaining
        ad_results = async_res.get()
        response_dict['ad_data']=ad_results

    return JsonResponse(response_dict, status=200)



###########  CHARTS ############

# Return Messages #
CSV_ERROR = "Error Generating CSV!"
###################

# Defines view for getting EPA concentration vs time chart data
def get_conc_vs_time(request):
    pollutant = request.GET.get('poll').lower()
    db_type = poll_to_db_type(pollutant)
    if not db_type: return HttpResponse('Error', status=400)

    site_id = int(request.GET.get('site'))

    sd = request.GET.get('start')
    ed = request.GET.get('end')
    end_date = datetime.strptime(ed, "%m/%d/%Y") if ed else datetime.now()
    start_date = datetime.strptime(sd, "%m/%d/%Y") if sd else end_date-timedelta(days=365)

    # Get data ordered by date
    objs = db_type.objects(aqs_site_id=site_id, date__gte=start_date, date__lte=end_date).order_by('date')
    results = []
    # Make an array of two element arrays as [date,concentration]
    for obj in objs:
        results.append([obj.date,obj.concentration])
    return JsonResponse({'data':results})

# defines the view for generating either EPA or Device CSV data
def create_csv(request):
    source = request.GET.get('source').lower()

    sd = request.GET.get('start')
    ed = request.GET.get('end')
    end_date = datetime.strptime(ed, "%m/%d/%Y") if ed else datetime.now()
    start_date = datetime.strptime(sd, "%m/%d/%Y") if sd else end_date-timedelta(days=365)

    if source == EPA:
        poll = request.GET.get('poll').lower()
        db_type = poll_to_db_type(poll)
        if db_type:
            response = generate_epa_csv(poll,db_type,start_date,end_date)
        else: return HttpResponse(BAD_REQUEST,status=400)
    elif source == DEVICE:
        response = generate_device_csv(start_date,end_date)
    else: return HttpResponse(BAD_REQUEST,status=400)
    return response if response else HttpResponse(CSV_ERROR,status=500)

# generator for epa data csv's
#TODO give Location fields
def generate_epa_csv(poll,db_type,start_date,end_date):
    # get all the headers except for id
    headers = [head.replace('_',' ').title() for head in db_type._fields.keys() if head != 'id']
    headers.sort()
    # make sure id is at the beginning
    headers = ['ID'] + headers
    response = HttpResponse(content_type='text/csv',status=200)
    dt=datetime.now()
    filename = "{0}_{1}-{2:02d}-{3:02d}-{4:02d}:{5:02d}:{6:02d}.csv".format(\
        poll.upper(),dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second)
    response['Content-Disposition'] = 'attachment; filename='+filename
    writer = csv.writer(response)
    writer.writerow(headers)
    for obj in db_type.objects(date__gte=start_date, date__lte=end_date):
        obj_dict = dict(obj.to_mongo())
        writer.writerow([obj_dict['_id']]+[obj_dict[key] for key in sorted(obj_dict) if key != '_id'])
        return response

# generator for device data csv's
#TODO give query fields
def generate_device_csv(start_date, end_date):
    # get all the headers except for id and device id
    headers = [head.replace('_',' ').title() for head in DeviceRecord._fields.keys() if head not in ('id','device_id','gps')]
    headers.sort()
    # make sure device id and id are at the beginning
    headers = ['Device ID','ID']+headers + ['Latitude','Longitude','Altitude','Altitude Units','Satellites','Device Time']
    response = HttpResponse(content_type='text/csv')
    dt=datetime.now()
    filename = "{0}_{1}-{2:02d}-{3:02d}-{4:02d}:{5:02d}:{6:02d}.csv".format(\
        'AIRDATA',dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second)
    response['Content-Disposition'] = 'attachment; filename='+filename
    writer = csv.writer(response)
    writer.writerow(headers)
    for obj in DeviceRecord.objects(datetime__gte=start_date, datetime__lte=end_date):
        obj_dict = dict(obj.to_mongo())
        row = [obj_dict[key] for key in sorted(obj_dict) if key not in ('_id','device_id','gps')]
        row = [obj_dict['device_id'],obj_dict['_id']]+row
        row += [obj_dict['gps']['latitude'],
                obj_dict['gps']['longitude'],
                obj_dict['gps']['altitude'],
                obj_dict['gps']['altitude_units'],
                obj_dict['gps']['number_satellites'],
                obj_dict['gps']['unix_time']]
        writer.writerow(row)
    return response

# helper function to translate pollutant type strings to their EPA DB object
def poll_to_db_type(poll):
    if poll == PM2_5:
        return EpaPm25
    elif poll == PM10:
        return EpaPm10
    elif poll == OZONE:
        return EpaOzone
    else: return None #handle error outside
